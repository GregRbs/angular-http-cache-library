import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { forkJoin, of, Observable } from 'rxjs';
import { map, catchError, timeout } from 'rxjs/operators';
import { HttpOptions } from './interfaces/http-options';

@Injectable({
  providedIn: 'root'
})
export class HttpService {

  constructor(private http: HttpClient) { }

  get<T>(url: string, options: HttpOptions = {}): Observable<T> {
    const api = this.http.get<T>(url, options)
      .pipe(
        timeout(2000),
        catchError(err => of(null))
      );

    const cache = of(JSON.parse(localStorage.getItem(url)));

    return forkJoin(cache, api)
      .pipe(
        map(res => {
          if (res[1]) {
            localStorage.setItem(url, JSON.stringify(res[1]));
            return res[1];
          }

          this.retryApiCall<T>(url, options);
          return res[0];
        })
      );
  }

  private retryApiCall<T>(url: string, options: any = {}) {
    this.http.get<T>(url, options)
      .pipe(
        timeout(10000),
        catchError(err => of(null))
      )
      .subscribe(res => {
        if (res) {
          localStorage.setItem(url, JSON.stringify(res));
        }
      })
  }
}
