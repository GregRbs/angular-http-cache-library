# Angular Http Cache Library

This library aims to provide an easy and performant caching system as a layer between the HttpClient and your components.

## Installation

Be sure to install the version corresponding to your Angular project.

```
# Install with yarn
yarn add @gregrbs/angular-http

# Install with npm
npm install --save @gregrbs/angular-http
```

## Usage

Instead of importing the HttpClient from Angular to make your GET requests as follow:
```import { HttpClient } from '@angular/common/http';```

Import this package and use it as you would do with the normal angular package.
```
import { HttpClient } from '@angular/common/http';

class ArticlesComponent {

    articles: Articles[] = [];

    constructor(private http: HttpService) {}

    getArticles() {
        this.http.get<Article[]>('https://url/articles')
        .subscribe(articles => this.articles = articles);
    }
}
```